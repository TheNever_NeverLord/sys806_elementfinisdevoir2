from math import *
import numpy as np
import matplotlib.pyplot as plt
from os import system, name
from fem_class import *
from fem_data import *
from fem_error import *
from fem_fonction import *
  
class Switcher(object):
  def indirect(self,i):
    method_name='question_'+str(i)
    method=getattr(self,method_name,lambda :print("Invalid\nEntrée 1, 2 ou 3 pour acceder à une question ou q pour quitter"))
    return method()

  def question_1(self):
    nbElems,nNodesPerElem,gaussPointNumber = inputParameters()
    fem = assembleAndSolveProbleme(nbElems,nNodesPerElem,gaussPointNumber)
    plotSolution(fem,False)

  def question_2(self):
    print("Cette fonction utilise beaucoup de resource pendant plusieurs minute")
    res = input("Voulez vous poursuivre ? [o,n]")
    if(res == 'o'):
      errorNormeL2()

  def question_3(self):
    nbElems,nNodesPerElem,gaussPointNumber = inputParameters()
    fem = assembleAndSolveProblemeVariationel(nbElems,nNodesPerElem,gaussPointNumber)
    plotSolution(fem,True)

  def question_q(self):
    global k 
    k = False

def clear(): 
  if name == 'nt': 
    _ = system('cls')  
  else: 
    _ = system('clear') 

k = True
def main():
  clear()
  s = Switcher()
  global k
  while(k == True):
    cmd = input("choix de la question : ")
    s.indirect(cmd)

if __name__ == "__main__":
  main()