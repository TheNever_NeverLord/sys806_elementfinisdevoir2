from math import *
import numpy as np
import matplotlib.pyplot as plt

from fem_data import *

def Gauss(gaussPointNumber):
  ksi,w =0,0
  if(gaussPointNumber==1):
    ksi = np.array([0])
    w = np.array([2])
  if(gaussPointNumber==2):
    ksi = np.array([-1/sqrt(3),1/sqrt(3)])
    w = np.array([1,1])
  if(gaussPointNumber==3):
    ksi = np.array([0,-sqrt(3/5.),sqrt(3/5.)])
    w = np.array([8/9.,5/9.,5/9.])
  if(gaussPointNumber==4):
    ksi = np.array([sqrt((3-2*sqrt(6/5.))/7.),-sqrt((3-2*sqrt(6/5.))/7.),sqrt((3+2*sqrt(6/5.))/7.),-sqrt((3+2*sqrt(6/5.))/7.)])
    w = np.array([0.5+(1/6.)*1./sqrt(6/5.),0.5+(1/6.)*1./sqrt(6/5.),0.5-(1/6.)*1./sqrt(6/5.),0.5-(1/6.)*1./sqrt(6/5.)])
  return(ksi,w)


class mesh_cl():
  def setConnectionTab(self):
    ind_node = 0
    for e in range(0,self.nbElems):
      for nn in range(0,self.nNodesPerElem):
        self.connec[e,nn] = ind_node
        ind_node = ind_node + 1
      ind_node = ind_node - 1
      self.area[e] = abs(self.nodes[self.connec[e,0]] - self.nodes[self.connec[e,self.nNodesPerElem-1]])

  def __init__(self,a,b,nbElems,nNodesPerElem):
    self.a = a
    self.b = b
    self.nbElems = nbElems
    self.nNodesPerElem = nNodesPerElem
    self.nNodes = self.nNodesPerElem*nbElems - (nbElems-1)
    self.nodes = np.linspace(a,b,self.nNodes)  # Noeuds du maillage
    self.connec = np.zeros((nbElems, self.nNodesPerElem), dtype=np.int32)    # Connectivité des éléments
    self.area = np.zeros(nbElems, dtype=np.float64)        # Aire des éléments (longueur) 
    self.setConnectionTab()



class fem_cl():
  def __init__(self, shapeFunction, mesh, gaussPointNumber):
    self.shapeFunction = shapeFunction  
    self.mesh = mesh                    
    self.gaussPointNumber = gaussPointNumber

    # Matrice du système
    self.K = np.zeros((self.mesh.nNodes, self.mesh.nNodes), dtype=np.float64)
    self.F = np.zeros(self.mesh.nNodes, dtype=np.float64)
    self.solution = np.zeros(self.mesh.nNodes, dtype=np.float64)
    self.ksi, self.w = Gauss(self.gaussPointNumber)

  def AVarible(self,positionElement,ig):
    pos2 = (positionElement+1)*self.mesh.nNodesPerElem-(positionElement+1)
    pos1 = positionElement*self.mesh.nNodesPerElem-positionElement
    ksi = self.ksi[ig]
    x= 0.5*(1-ksi)*self.mesh.nodes[pos1]+ 0.5*(1+ksi)*self.mesh.nodes[pos2]
    y =((AL-A0)/L)*x +A0
    return y
 
  def assemble(self):
    for e in range(0, self.mesh.nbElems):
      # Intégration numérique
      J = self.mesh.area[e]/2.                        
      kloc = np.zeros((self.mesh.nNodesPerElem,self.mesh.nNodesPerElem), dtype=np.float64)    
      floc = np.zeros((self.mesh.nNodesPerElem), dtype=np.float64)
      
      for ig in range(0,self.gaussPointNumber):   
        f, df = self.shapeFunction(self.ksi[ig]) 
        assert(self.mesh.nNodesPerElem == len(f))
        assert(len(df) == len(f))
        # dfref = df*J (len(fref) = len(dfref) = self.mesh.nNodesPerElem)
        df=df/J
        kloc = kloc + J*self.w[ig]*A*E*np.tensordot(df,df,0)
        floc = floc + J*self.w[ig]*(-RHO*G*A)*f 
        
      # Ecrit la matrice locale dans la matrice globale 
      for i in range(0, self.mesh.nNodesPerElem):
          ii = self.mesh.connec[e,i]
          for j in range(0, self.mesh.nNodesPerElem):
              jj = self.mesh.connec[e,j]
              self.K[ii,jj] = self.K[ii,jj] + kloc[i,j] 
          self.F[ii] = self.F[ii] + floc[i]

  def assemble2(self):
    for e in range(0, self.mesh.nbElems):
      # Intégration numérique
      J = self.mesh.area[e]/2.                        
      kloc = np.zeros((self.mesh.nNodesPerElem,self.mesh.nNodesPerElem), dtype=np.float64)    
      floc = np.zeros((self.mesh.nNodesPerElem), dtype=np.float64)
      
      for ig in range(0,self.gaussPointNumber):   
        f, df = self.shapeFunction(self.ksi[ig]) 
        assert(self.mesh.nNodesPerElem == len(f))
        assert(len(df) == len(f))
        # dfref = df*J (len(fref) = len(dfref) = self.mesh.nNodesPerElem)
        df=df/J
        a = self.AVarible(e,ig)
        kloc = kloc + J*self.w[ig]*a*E*np.tensordot(df,df,0)
        floc = floc + J*self.w[ig]*(-RHO*G*a)*f 
        
      # Ecrit la matrice locale dans la matrice globale 
      for i in range(0, self.mesh.nNodesPerElem):
          ii = self.mesh.connec[e,i]
          for j in range(0, self.mesh.nNodesPerElem):
              jj = self.mesh.connec[e,j]
              self.K[ii,jj] = self.K[ii,jj] + kloc[i,j] 
          self.F[ii] = self.F[ii] + floc[i]
        

  def solve(self):
    self.solution = np.linalg.solve(self.K,self.F)

  def recons_solution(self, nv):
    self.xv = np.linspace(self.mesh.a, self.mesh.b, nv)
    self.yv = np.zeros(nv)
    for i in range(0, nv):
        elem = -1
        for e in range(0,self.mesh.nbElems):
          if((self.xv[i] >= self.mesh.nodes[self.mesh.connec[e,0]]) and (self.xv[i] < self.mesh.nodes[self.mesh.connec[e,self.mesh.nNodesPerElem-1]])):
            elem = e
            break
        #Ajouter la contribution de chaque shape fonction
        fx, dfx = self.shapeFunction((self.xv[i]-self.mesh.nodes[self.mesh.connec[elem,0]])*(2./self.mesh.area[elem])-1.)
        for j in range(0, self.mesh.nNodesPerElem):
            self.yv[i] = self.yv[i] + self.solution[self.mesh.connec[elem,j]]*fx[j]
    
  def boundary_conditions(self):
    nn = self.mesh.nNodes
    self.K[0,:] = 0
    self.K[0,0] = 1
    self.F[0] = U0
    self.F[nn-1] = self.F[nn-1] - M*G
