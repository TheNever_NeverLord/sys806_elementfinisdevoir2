from math import *
import numpy as np
import matplotlib.pyplot as plt

from fem_class import *
from fem_data import *
from fem_fonction import *

def SolutionExact(x):
  solution = (RHO*G*x*x)/(2*E) - (G*(M+RHO*A*L)*x)/(A*E)
  return solution

def SolveProbleme(nbElems):
  nNodesPerElem = 3
  gaussPointNumber = 4
  shapeFunction = setShapeFunction(nNodesPerElem)
  mesh = mesh_cl(0,L,nbElems,nNodesPerElem) #L= longueur de la colonne
  fem = fem_cl(shapeFunction, mesh, gaussPointNumber)
  fem.assemble()
  fem.boundary_conditions()
  fem.solve()
  fem.recons_solution(NV)
  return fem

def integraleSimpsonFem(fem,sol):
  denominateur = (NV-1)/2
  h = L/denominateur
  count = 0
  for i in range(1,NV-1):
    count += fem.yv[i-1] + 4*fem.yv[i] + fem.yv[i+1] -sol[i-1]-4*sol[i] - sol[i+1]
  count = (count * h)/6
  return count

def calculErreur(fem,sol):
  erreur = integraleSimpsonFem(fem,sol)
  erreur = sqrt(erreur)
  return erreur

def calculErreurGauss(fem):
  npg =4
  ksi,w = Gauss(npg)
  count = 0
  for e in range(fem.mesh.nbElems):
    i = fem.mesh.connec[e,0]
    j = fem.mesh.connec[e,1]
    x1 =fem.mesh.nodes[i]
    x2 =fem.mesh.nodes[j]
    Le = x2 -x1
    u1 = fem.solution[i]
    u2 = fem.solution[j]
    for k in range(1,npg):
      xg= 0.5*(1-ksi[k])*x1+ 0.5*(1+ksi[k])*x2
      solutionExact = SolutionExact(xg)
      ug = 0.5*(1-ksi[k])*u1+ 0.5*(1+ksi[k])*u2
      jacobien = Le/2
      count += jacobien*w[k]*(ug-solutionExact)**2

    erreur = sqrt(count)
  return erreur


def errorNormeL2():
  err = []
  n = []
  #sol = calculSolutionExact()
  for i in range (1,1000):
    deplacement = SolveProbleme(i)
    err.append(calculErreurGauss(deplacement))
    n.append(i)

  for i in range (0,999):
    err[i] = log10(err[i])
    n[i] = log10(n[i])
  
  tmp = np.polyfit(n,err,1)
  print("ordre de convergence log10", -tmp[0])
  plt.figure(num='erreur en fonction du nombre d\'element')
  plt.plot(n,err,label="erreur")
  plt.legend()
  plt.show()