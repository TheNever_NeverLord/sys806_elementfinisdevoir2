from math import *
import numpy as np

from fem_class import *
from fem_data import *
from fem_error import *


def calculSolutionExact():
  x = np.linspace(0,L,NV)
  solution = (RHO*G*x*x)/(2*E) - (G*(M+RHO*A*L)*x)/(A*E)
  return solution


def P1(x):
  assert(x>-1 or x<1)
  fx1  = (1-x)/2.
  dfx1 = -1/2.
  fx2  = (1+x)/2.
  dfx2 = 1/2.

  return(np.array([fx1,fx2]),np.array([dfx1,dfx2])) 

def P2(x):
  assert(x>-1 or x<1)
  fx1 = x*(x-1)/2.
  dfx1 = (2*x-1)/2.
  fx2 = x*(x+1)/2.
  dfx2 = (2*x+1)/2.
  fx3 = -(x-1)*(x+1)
  dfx3 = -2*x

  return(np.array([fx1,fx3,fx2]),np.array([dfx1,dfx3,dfx2]))

def setShapeFunction(nNodesPerElem):
  if(nNodesPerElem == 2):
    shapeFunction = P1
  elif(nNodesPerElem == 3):
    shapeFunction = P2

  return shapeFunction

def assembleAndSolveProbleme(nbElems,nNodesPerElem, gaussPointNumber):
  shapeFunction = setShapeFunction(nNodesPerElem)
  mesh = mesh_cl(0,L,nbElems,nNodesPerElem) #L= longueur de la colonne
  fem = fem_cl(shapeFunction, mesh, gaussPointNumber)
  fem.assemble()
  fem.boundary_conditions()
  fem.solve()
  fem.recons_solution(NV)
  return fem

def assembleAndSolveProblemeVariationel(nbElems,nNodesPerElem, gaussPointNumber):
  shapeFunction = setShapeFunction(nNodesPerElem)
  mesh = mesh_cl(0,L,nbElems,nNodesPerElem) #L= longueur de la colonne
  fem = fem_cl(shapeFunction, mesh, gaussPointNumber)
  fem.assemble2()
  fem.boundary_conditions()
  fem.solve()
  fem.recons_solution(NV)
  
  return fem

def plotSolution(fem,bool):
  plt.figure(num = "solution")
  x = np.linspace(0,L,NV)
  plt.plot(fem.xv, fem.yv, label = "solution FEM")
  if(bool == True):
    pass
  else:
    solutionExact =calculSolutionExact()
    plt.plot(x,solutionExact,label="solution exacte")
  plt.legend()
  plt.show()


def strToInt(nbElems,nNodesPerElem,gaussPointNumber):
  nbElems = int(nbElems)
  nNodesPerElem = int(nNodesPerElem)
  gaussPointNumber = int(gaussPointNumber) 

  return nbElems,nNodesPerElem,gaussPointNumber

def inputParameters():
  nbElems = input("entrez le nombres d\'element ")
  nNodesPerElem = input("entrez le nombres de noeuds par element ")
  gaussPointNumber = input("entrez le nombres de point de gauss ")
  nbElems,nNodesPerElem,gaussPointNumber =strToInt(nbElems,nNodesPerElem,gaussPointNumber)
  k = True
  while(k):
    if(nbElems <1 ):
      print("le nombres d\'element doit etre superieur à 1")
      nbElems = input("entrez le nombres d\'element ")
      nbElems = int(nbElems)
    else:
      k = False

  k = True
  while(k):
    if(nNodesPerElem != 2 and nNodesPerElem !=3):
      print("le nombres de noeuds par element doit etre 2 ou 3")
      nNodesPerElem = input("entrez le nombres de noeuds par element ")
      nNodesPerElem = int(nNodesPerElem)
    else:
      k = False

  k = True
  while(k):
    if(gaussPointNumber < 1 or gaussPointNumber >4 or gaussPointNumber == 1):
      if(gaussPointNumber == 1 and nNodesPerElem==3):
        print("le nombres de point de gauss doit etre compris entre 2 et 4")
        gaussPointNumber = input("entrez le nombres de point de gauss ")
      elif(gaussPointNumber == 1 and nNodesPerElem==2):
        k =False
      else:  
        print("le nombres de point de gauss doit etre compris entre 1 et 4")
        gaussPointNumber = input("entrez le nombres de point de gauss ")
      gaussPointNumber = int(gaussPointNumber)
    else:
      k = False

  return nbElems,nNodesPerElem,gaussPointNumber